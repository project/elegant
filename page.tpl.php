<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <meta name="AUTHOR" content="http://www.finex.org" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="KEYWORDS" content="KEYWORDS" />

  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>

  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>

</head>
<body>

<div id="secondary"> <!--START SECONDARY LINKS-->
  <?php if (isset($secondary_links)) { ?>
    <div class="column">
      <?php print theme('links', $secondary_links) ?>
    </div>
  <?php } ?>
</div> <!--END SECONDARY LINKS-->

<div id="header"> <!--START HEADER-->
  <div class="column">
    <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $directory ?>/img/hdr.jpg" alt="<?php print t('Home') ?>" /></a>
  </div>
</div> <!--END HEADER-->

<div id="primary"> <!--START PRIMARY LINKS-->
  <?php if (isset($primary_links)) { ?>
    <div class="column">
      <?php print theme('links', $primary_links) ?>
    </div>
  <?php } ?>
</div> <!--END PRIMARY LINKS-->

<div id="page"> <!--START PAGE-->

  <div class="column"> <!--START COLUMN (page)-->

    <div class="wrapper"></div>

    <div id="sidebar">
      <?php if ($sidebar_left) { ?>
        <?php print $sidebar_left ?>
      <?php } ?>

      <?php if ($sidebar_right) { ?>
        <?php print $sidebar_right ?>
      <?php } ?>
    </div>

    <div id="main">

      <?php if ($breadcrumb) { ?>
        <div id="breadcrumb">
          <?php print $breadcrumb ?>
        </div>
      <?php } ?>

      <?php print $help ?>
      <?php print $messages ?>

      <?php if ($title) { ?>
        <h1 class="title"><?php print $title ?></h1>
      <?php } ?>

      <?php if ($tabs) { ?>
        <div class="tabs">
          <?php print $tabs ?>
        </div>
      <?php } ?>

      <?php print $content; ?>

    </div>

    <div class="wrapper"></div>

  </div> <!--END COLUMN (page)-->

</div> <!--END PAGE-->

<div class="wrapper"></div>

<div id="footer"> <!--START FOOTER-->

  <div class="column"> <!--START COLUMN (footer)-->

    <?php print $footer_message ?>

    <?php print $feed_icons ?>

    <div id="author" > <!-- PLEASE DON'T REMOVE THIS SECTION -->
      Elegant template by <a href="http://www.finex.org">FiNe<strong>X</strong><em>design</em></a>
      <a href="http://www.finex.org">
        <img src="<?php print base_path() . $directory ?>/img/finex_design.gif" alt="FiNeXdesign" style="border:0"/>
      </a>
    </div>

  </div> <!--END COLUMN (footer)-->

</div> <!--END FOOTER-->

<?php print $closure ?>


</body>
</html>
